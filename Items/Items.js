import React from 'react';
import {Text, View, StyleSheet, ScrollView} from "react-native";
import {connect} from "react-redux";
import {getItems} from '../store/actions';
import Item from "../Item/Item";

class Items extends React.Component {
    componentDidMount() {
        this.props.getItems();
    }

    render() {

        return (
            <ScrollView>
                <View style={styles.container}>
                    {this.props.items.map(item => (
                        <Item
                            key={item.data.id}
                            image={item.data.thumbnail}
                            title={item.data.title}
                        />
                    ))}

                </View>
            </ScrollView>
        )
    }
};

const mapStateToProps = state => ({
    items: state.items
});

const mapDispatchToProps = dispatch => ({
    getItems: () => dispatch(getItems())
});

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        width: '80%',
        marginLeft: 30
    }
})


export default connect(mapStateToProps, mapDispatchToProps)(Items);