import React from 'react';
import Items from "./Items/Items";
import {Provider} from "react-redux";
import thunk from 'redux-thunk';
import {applyMiddleware, createStore} from "redux";
import reducer from "./store/reducer";
import axios from 'axios';

axios.defaults.baseURL = 'https://www.reddit.com/r';

export default class App extends React.Component {
    render() {
        const store = createStore(reducer, applyMiddleware(thunk));
        return (
            <Provider store={store}><Items/></Provider>
        );
    }
}
