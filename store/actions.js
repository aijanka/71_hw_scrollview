import axios from 'axios';
import * as actionTypes from './actionTypes';

export const itemsRequest = () => ({type: actionTypes.ITEM_REQUEST});
export const itemsSuccess = items => ({type: actionTypes.ITEM_SUCCESS, items});
export const itemsFailure = error => ({type: actionTypes.ITEM_FAILURE, error});

export const getItems = () => {
    return (dispatch, getState) => {
        dispatch(itemsRequest());
        axios.get('/pics.json').then(response => {
            const result = response.data; // берем из axios
            const children = result.data.children;
            dispatch(itemsSuccess(children));
        }, error => {
            dispatch(itemsFailure(error));
        })
    }

};