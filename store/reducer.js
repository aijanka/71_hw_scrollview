import * as actionTypes from './actionTypes';

const initialState = {
    items: [],
    loading: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.ITEM_REQUEST:
            return {...state, loading: true};
        case actionTypes.ITEM_SUCCESS:
            return {...state, items: action.items, loading: false};
        case actionTypes.ITEM_FAILURE:
            return {...state, error: action.error, loading: false};
        default :
            return state;
    }
};

export default reducer;