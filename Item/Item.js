import React from 'react';
import {Image, StyleSheet, Text, View} from "react-native";

class Item extends React.Component {
    render() {
        return (
            <View style={styles.item}>
                <Image
                    style={{width: 50, height: 50}}
                    source={{uri: this.props.image}}
                />
                <Text style={styles.text}>{this.props.title}</Text>
            </View>
        );
    }
}

export default Item;

const styles = StyleSheet.create({
    item: {
        margin: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: {
        marginLeft: 10
    }
})